/**
 * Licensed under MIT
 */
(function ($) {
    var settings = {};

    $.fn.scaleToFit = function (options) {

        settings = $.extend({
            width: true,
            height: false,
            grow: false,
        }, options);

        $(window).resize(this, rescale);

        return doScale(this);
    };

    function rescale(event) {
        return doScale(event.data);
    }

    function doScale(elements) {
        return elements.each(function (index, element) {
            var $element=$(element);
            $element.css('transform', '');
            $element.css('display','inline-block');

            var width = $element.outerWidth();
            var height = $element.outerWidth();

            var $parent = $element.parent();
            var parentWidth = $parent.width();
            var parentHeight = $parent.height();

            var scaleX = width>0?parentWidth / width:1;
            var scaleY = height>0?parentHeight / height:1;

            if (!settings.grow) {
                scaleX = Math.min(1, scaleX);
                scaleY = Math.min(1, scaleY);
            }

            if (settings.width && settings.height) {
                scaleX = scaleY = Math.min(scaleX, scaleY);
            }
            if (!settings.width && settings.height) {
                scaleX = scaleY;
            }
            if (settings.width && !settings.height) {
                scaleY = scaleX;
            }
            if (!settings.width && !settings.height) {
                scaleX = scaleY = 1;
            }


            $element.css({
                transform: 'scale('+scaleX+','+scaleY+')',
                "transform-origin": 'top left',
            });
        });
    }

}(jQuery));